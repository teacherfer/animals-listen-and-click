var animals = ["Zebra", "Camel", "Tortoise", "Giraffe", "Monkey", "bird", "Kangaroo", "Lion", "Hippo", "Snake", "Cat", "Dog", "Baboon", "Bear", "Buffalo", "Deer", "Eagle", "Elephant", "Flamingo", "Gorilla", "Panther", "Rhino", "Wolf", "Wildboar", "Tiger", "Snowleopard"];
var length = animals.length;
var unselectedAnimals = ["Zebra", "Camel", "Tortoise", "Giraffe", "Monkey", "bird", "Kangaroo", "Lion", "Hippo", "Snake", "Cat", "Dog", "Baboon", "Bear", "Buffalo", "Deer", "Eagle", "Elephant", "Flamingo", "Gorilla", "Panther", "Rhino", "Wolf", "Wildboar", "Tiger", "Snowleopard"];
var correctAnswersCounters;
var incorrectAnswersCounters;
var posibleAnswers = [];
var currentPosition = -1;
var currentIteration = 1;
var speech = null;
var counter = 0;
var answered = false;

//showNavigationButtons();
initAnswersCountersArrays();
randomizeQuestion();
//showAnswersCounters();

function marcador() {
    for (let index = 0; index < length; index++) {
        console.log(animals[index] + " -> Aciertos " + correctAnswersCounters[index] + " Fallos " + incorrectAnswersCounters[index]);
    }
}

function initAnswersCountersArrays() {
    correctAnswersCounters = new Array(length).fill(0);
    incorrectAnswersCounters = new Array(length).fill(0);
}

function randomizeQuestion() {
    var rand = Math.floor(Math.random() * unselectedAnimals.length);
    currentPosition = animals.indexOf(unselectedAnimals[rand]);
    loadSpeech();
    unselectedAnimals.splice(rand, 1);
    randomizeAnswers();
}

function randomizeAnswers() {
    posibleAnswers = [];
    var animalsClone = animals.slice(0);
    posibleAnswers.push(animals[currentPosition]);
    animalsClone.splice(currentPosition, 1);
    animalsClone.sort(() => .5 - Math.random());
    var aux = animalsClone.slice(0, 2);
    posibleAnswers = posibleAnswers.concat(aux);
    posibleAnswers.sort(() => .5 - Math.random());
    showPosibleAnswers();
}

function showPosibleAnswers() {
    for (let index = 0; index < posibleAnswers.length; index++) {
        const element = document.createElement("img");
        element.src = "img/" + posibleAnswers[index].toLowerCase() + ".jpg";
        element.alt = posibleAnswers[index];
        element.classList.add("answer");
        document.querySelector(".answers-container").appendChild(element);
    }
    loadAnswersListeners();
}

function removePosibleAnswers() {
    document.querySelector(".answers-container").innerHTML = "";
}

function loadAnswersListeners() {
    var answers = document.querySelectorAll(".answer");
    for (let i = 0; i < answers.length; i++) {
        answers[i].addEventListener("click", answerSelected)
    }
}

function answerSelected(e) {
    if (!answered) {
        answered = true;
        var selected = e.target.alt;
        var el = document.querySelector(".answer-text");
        el.style.opacity = 1;
        var index = animals.indexOf(selected);
        if (index == currentPosition) {
            el.style.color = 'green';
            correctAnswersCounters[index]++;
        } else {
            el.style.color = 'red';
            incorrectAnswersCounters[currentPosition]++;
        }

        el.textContent = selected;
        var s = el.style,
            step = 25 / (1500 || 300);
        s.opacity = s.opacity || 1;
        (function fade() {
            (s.opacity -= step) < 0 ? s.textContent = "" : setTimeout(fade, 25);
        })();
        setTimeout(nextQuestion,1000);
    }

}

function initializeItem() {
    loadImage();
    loadSpeech();
    loadQuestionCounter();
}

function loadQuestionCounter() {
    document.querySelector(".question-counter").textContent = currentIteration + " / " + length;
}

function loadImage() {
    document.querySelector(".image").src = "img/" + animals[currentPosition].toLowerCase() + ".jpg";
}

function loadSpeech() {
    speech = new SpeechSynthesisUtterance(animals[currentPosition]);
    speech.lang = 'en-UK';
    document.querySelector(".fa-volume-up").addEventListener("click", playSpeech);
}

function playSpeech() {
    window.speechSynthesis.speak(speech);
}

function nextQuestion() {
    answered = false;
    removePosibleAnswers();
    randomizeQuestion();
    if (unselectedAnimals.length == 0)
        unselectedAnimals = animals.slice(0);
}
